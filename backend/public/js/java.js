$("#submit-form").on('click',function(e){
	e.preventDefault();
	var kosong=0;
   			 	$('#form_input').find('input').each(function(){
    				if($(this).prop('required')){
        				
        				if( ! $(this).val() ){
        					kosong=1;
        					$(this).focus();
        					return false;
        				}
    					
    					} 
				});
                $('#form_input').find('select').each(function(){
                    if($(this).prop('required')){
                        
                        if( ! $(this).val() ){
                            kosong=1;
                            $(this).focus();
                            return false;
                        }
                        
                        } 
                });
                $('#form_input').find('textarea').each(function(){
                    if($(this).prop('required')){
                        
                        if( ! $(this).val() ){
                            kosong=1;
                            $(this).focus();
                            return false;
                        }
                        
                        } 
                });
				if (kosong==1){
					alert('semua form harus di isi');
				}else {
				$.ajax({
            			type:"POST",
            			url: $("#form_input").attr('action'),
            			data:$("#form_input").serialize(),
                        beforeSend:function(){
                            $(".loading").css("display","block");
                        },
            			success: function(response){
            			   	$(".loading").css("display","none");  
                            $("#row-hasil").html(response); 
                            $("#form_input")[0].reset();   					
            		},
                     error: function(){
                     		$(".loading").css("display","none"); 
                        alert('gagal menyimpan data');
                            }
            	});
				}
});