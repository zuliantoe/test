<!DOCTYPE html>
<html>
<head>
    <title>Front End</title>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form id="form_input" action="{{ url('simpan_data') }}" class="form-horizontal form-input">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="col-md-2 col-md-offset-1 control-label">Nama</label>
                    <div class="col-md-6">
                        <input type="text" name="nama" id="nama" class="form-control" placeholder="Nama" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 col-md-offset-1 control-label">Jenis Kelamin</label>
                    <div class="col-md-2">
                        <select name="jenis_kelamin" id="jeniskelamin" class="form-control" required> 
                            <option>Pilih</option>
                            <option value="Laki-laki">Laki-laki</option>
                            <option value="Perempuan">Perempuan</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 col-md-offset-1 control-label">Keterangan</label>
                    <div class="col-md-6">
                        <textarea name="keterangan" class="form-control" required=""></textarea>
                    </div>
                </div>
                <div class="col-md-2 col-md-offset-3">
                    <span class="loading" style="display: none;">Sedang Menyimpan...</span>
                </div>
                <div class="col-md-1 col-md-offset-7">
                    <button type="button" id="submit-form">Simpan</button>
                </div>
                
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-1">
            <table class="table table-hasil">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Jenis Kelamin</th>
                        <th>Keterangan</th>
                    </tr>
                </thead>
                <tbody id="row-hasil">
                    
                </tbody>
            </table>
        </div>
    </div>
</div>



<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
 <!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script type="text/javascript" src="js/java.js" ></script>
</body>
</html>